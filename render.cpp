internal void
PushRectangle(RenderList *rlist, Rectf rect, v4 color)
{
    if(rlist->num_entries < rlist->max_entries)
    {
        RenderEntry *entry = &rlist->entries[rlist->num_entries];
        entry->type = Render_rectangle;
        entry->rect = rect;
        entry->color = color;
    
        rlist->num_entries++;
    }
    else
    {
        // TODO: Logging!
        
    }
}

internal void
PushTexture(RenderList *rlist, Rectf rect, v4 color, Rectf uvst, RenderTexture *texture)
{
    if(rlist->num_entries < rlist->max_entries)
    {
        RenderEntry *entry = &rlist->entries[rlist->num_entries];
        entry->type = Render_texture;
        entry->rect = rect;
        entry->color = color;
        entry->uvst = uvst;
        entry->texture = *texture;
        
        rlist->num_entries++;
    }
    else
    {
        // TODO: Logging!
    }
}
