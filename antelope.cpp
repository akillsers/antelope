#include "antelope.h"

void GameUpdateAndRender(GameMemory *memory, Input *input, RenderList *rlist)
{
    GameState *game_state = (GameState *)memory->storage;
    
    if(!memory->is_initialized)
    {
        game_state->player_pos.x = 100;
        game_state->player_pos.y = 100;
        game_state->player_vel.x = 0;
        game_state->player_vel.y = 0;
        memory->is_initialized = true;
        printf("Warning: repeat initialization!");
    }

    f32 speed = 100.0f;
    
    v2 movement_dir = {};
    if(input->buttons[Button_left].ended_down)
    {        
        movement_dir.x = -1.0f;
    }
    
    if(input->buttons[Button_right].ended_down)
    {
        movement_dir.x = 1.0f;
    }
    
    if(input->buttons[Button_up].ended_down)
    {        
        movement_dir.y = -1.0f;       
    }
    
    if(input->buttons[Button_down].ended_down)
    {
        movement_dir.y = 1.0f;
    }
    f32 player_speed = Norm(game_state->player_vel);
    movement_dir = Normalize(movement_dir);
    f32 movement_magnitude = 800.0f;
    v2 movement_force = movement_dir * 800.0f;
    
    v2 air_drag = {0,0};
    v2 drag_dir = Normalize(-game_state->player_vel);    
    f32 taper_speed = 750.0f;
    if(player_speed >= taper_speed)
    {
        air_drag = drag_dir * 40.0f * (player_speed - taper_speed) * (player_speed - taper_speed);
    }

    v2 drag_force = drag_dir * 600.0f;
    
    
    v2 jet_dir = Normalize(game_state->player_pos - input->mouse_pos);
    v2 jet_force = {0.0f, 0.0f};
    v2 repel_force = {0.0f, 0.0f};
    
    if(input->buttons[Button_shoot].ended_down)
    {
        jet_force = jet_dir * 1200.0f;
        

    }
    
    if(input->buttons[Button_action].ended_down)
    {
        
        if(input->buttons[Button_shoot].ended_down)
        {
            drag_force = drag_force * 3.0f;
        }
        
        
        f32 repel_force_min_speed = 250.0f;
        if(player_speed >=  repel_force_min_speed )
        {
            f32 repel_magnitude = 4.0f * (player_speed -  repel_force_min_speed)*(player_speed -  repel_force_min_speed);
            
            if(repel_magnitude > 3600.0f)
            {
                repel_magnitude = 3600.0f;
            }
            
            v2 repel_dir = (input->buttons[Button_shoot].ended_down) ? -jet_dir : drag_dir;
            repel_force = repel_dir * repel_magnitude;
            // drag_force = drag_force * 1.0f * (player_speed - repel_force_min_speed);            
        }
    }
    
    v2 net_force = movement_force + drag_force + jet_force + repel_force + air_drag;
    f32 mass = 1.0f;
    
    game_state->player_vel += (net_force/mass) * input->dt;
    
    f32 player_vel_epsilon = 5.0f;
    if(Norm(game_state->player_vel) <= player_vel_epsilon)
    {
        game_state->player_vel.x = 0.0f;
        game_state->player_vel.y = 0.0f;
    }
    
    game_state->player_pos += game_state->player_vel * input->dt;
    printf("Player pos (%0.3f, %0.3f) vel (%0.3f, %0.3f) force (%0.3f, %0.3f)\n", game_state->player_pos.x, game_state->player_pos.y, game_state->player_vel.x, game_state->player_vel.y, net_force.x, net_force.y);

    
    Rectf player_rect = {};
    
    player_rect.x = game_state->player_pos.x;
    player_rect.y = game_state->player_pos.y;
    player_rect.width = 10;
    player_rect.height = 10;
    
    v4 color = {1.0f, 0.5f, 0.5f, 1.0f};
    
    PushRectangle(rlist, player_rect, color);
}
