#include "SDL.h"
#include <stdio.h>
#include <sys/mman.h>
#include <errno.h>

#include "common.h"
#include "antelope_platform.h"
#include "render.cpp"
#include "antelope_sdl_render.cpp"

#include "antelope.cpp"

ButtonID SDLScancodeToButton(SDL_Scancode scancode)
{
    ButtonID button = Button_null;

    switch(scancode)
    {
        case SDL_SCANCODE_W:
        {
            button = Button_up;
        } break;
        
        case SDL_SCANCODE_A:
        {
            button = Button_left;
        } break;
        
        case SDL_SCANCODE_D:
        {
            button = Button_right;
        } break;
        
        case SDL_SCANCODE_S:
        {
            button = Button_down;
        } break;
        
        case SDL_SCANCODE_SPACE:
        {
            button = Button_action;
        } break;
        
        default:
        {
        
        } break;
    }
    
    return button;
}

void *SDLAllocateMemory(u64 alloc_size)
{
    void *result = 0;
    result = mmap(0, alloc_size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if(result == MAP_FAILED)
    {
        // TODO: Handle this
        Assert(false);
        int i = errno;
        result = 0;
    }
    else
    {
        memset(result, 0, alloc_size);
    }
    
    return result;
}

int main(int argc, char* argv[])
{

    SDL_Window *window;                    // Declare a pointer

    SDL_Init(SDL_INIT_VIDEO);              // Initialize SDL2

    // Create an application window with the following settings:
    window = SDL_CreateWindow(
        "Antelope Project",                // window title
        SDL_WINDOWPOS_UNDEFINED,           // initial x position
        SDL_WINDOWPOS_UNDEFINED,           // initial y position
        640,                               // width, in pixels
        480,                                                      // height, in pixels
        SDL_WINDOW_OPENGL | SDL_WINDOW_MAXIMIZED  | SDL_WINDOW_RESIZABLE                // flags - see below
    );

    // Check that the window was successfully created
    if (window == NULL) {
        // In the case that the window could not be made...
        printf("Could not create window: %s\n", SDL_GetError());
        return 1;
    }
    
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED); 
    
    if(renderer == NULL)
    {
        // TODO: Logging
        printf("Could not create renderer: %s\n", SDL_GetError());
        return 1;
    }
    

    b32 is_running = true;

    GameMemory memory = {};
    u32 alloc_size = Megabytes(24);

    memory.is_initialized = false;    
    memory.storage_size = alloc_size;
    memory.storage = SDLAllocateMemory(alloc_size);

    if(memory.storage == 0)
    {
        // TODO: logging
    }
    
    f64 monitor_refresh_rate = 60.1f; // Let's say
    f64 target_seconds_per_frame = 1.0f/monitor_refresh_rate; //1.0f/monitor_refresh_rate;
    Input input = {};
    input.dt = target_seconds_per_frame;
    
    RenderList rlist = {};
    rlist.max_entries = 256;
    rlist.entries = (RenderEntry *) SDLAllocateMemory(rlist.max_entries * sizeof(RenderEntry));
    if(rlist.entries == 0)
    {
        // TODO: logging
        rlist.max_entries = 0;
    }
    //SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN);
    u64 last_counter = SDL_GetPerformanceCounter();
    u64 perf_freq = SDL_GetPerformanceFrequency();
    while(is_running)
    {
        // Do polling

        // Reset all key tra4sition counts
        for(u32 i = 0; i < Button_count; ++i)
        {
            input.buttons[i].transition_count = 0;
        }

        SDL_Event event;
        while(SDL_PollEvent(&event))
        {
            switch(event.type)
            {
                case SDL_KEYUP:
                {
                    SDL_KeyboardEvent key = event.key;
                    
                    ButtonID button = SDLScancodeToButton(key.keysym.scancode);
                    input.buttons[button].transition_count++;
                    input.buttons[button].ended_down = false;
                } break;
                
                case SDL_KEYDOWN:
                {
                    SDL_KeyboardEvent key = event.key;
                    
                    if(!key.repeat)
                    {
                        ButtonID button = SDLScancodeToButton(key.keysym.scancode);
                        input.buttons[button].transition_count++;
                        input.buttons[button].ended_down = true;
                    }
                } break;
                
                case SDL_MOUSEBUTTONUP:
                {
                    SDL_MouseButtonEvent mouse = event.button;
                    
                    if(mouse.button == SDL_BUTTON_LEFT)
                    {
                        input.buttons[Button_shoot].transition_count++;
                        input.buttons[Button_shoot].ended_down = false;
                    }
                    
                } break;
                
                case SDL_MOUSEBUTTONDOWN:
                {
                    SDL_MouseButtonEvent mouse = event.button;
                    
                    if(mouse.button == SDL_BUTTON_LEFT)
                    {
                        input.buttons[Button_shoot].transition_count++;
                        input.buttons[Button_shoot].ended_down = true;
                    }
                    
                } break;
            
                case SDL_QUIT:
                {
                    is_running = false;
                } break;
            }
        }
        
        // Update the mouse position
        int mouse_x, mouse_y;
        u32 buttons = SDL_GetMouseState(&mouse_x, &mouse_y);
        input.mouse_pos.x = mouse_x;
        input.mouse_pos.y = mouse_y;
        
        // Clear the render list
        rlist.num_entries = 0;
        GameUpdateAndRender(&memory, &input, &rlist);
        RenderListToOutput(renderer, &rlist);

        
        // TODO: We should push for the first guy
        u64 now = SDL_GetPerformanceCounter();
        while((now - last_counter) < target_seconds_per_frame*SDL_GetPerformanceFrequency())
        {
            now = SDL_GetPerformanceCounter();
        }
        last_counter = now;



        
    }

    // Clean up
    return 0;
}
