struct Button
{
    b32 ended_down;
    u32 transition_count;
};

enum ButtonID
{
    Button_null,
    
    Button_shoot,
    Button_action,
    Button_left,
    Button_right,
    Button_up,
    Button_down,
    Button_count    
};

struct Input
{
    Button buttons[Button_count];
    f64 dt;
    
    v2 mouse_pos;
    // Button mouse_buttons[Button_count];
};

#define MAX_RENDER_COMMAND

enum RenderEntryType
{
    Render_rectangle,
    Render_texture,
};

struct RenderTexture
{
    u32 width;
    u32 height;
    void *pixels;

    void *gpu_handle;
};

struct RenderEntry
{
    RenderEntryType type;

    i32 z;
    Rectf rect;
    v4 color;
    
    
    union
    {
        struct // Render_rectangle
        {
        };
        
        struct // Render_texture
        {
            Rectf uvst;
            RenderTexture texture;
        };
    };
};


struct RenderList
{
    u32 num_entries;
    u32 max_entries;
    RenderEntry *entries;
};

struct GameMemory
{
    b32 is_initialized;
    void *storage;
    u32 storage_size;
};


void GameUpdateAndRender(GameMemory *memory, Input *input, RenderList *rlist);
