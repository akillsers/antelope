//TODO: Any way to get rid of this dependency?
#include <stdint.h> // for uintptr_t
#include <math.h> // for sqrt

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef signed char i8;
typedef short i16;
typedef int i32;
typedef long long i64;

typedef i32 b32;
typedef i8 b8;
typedef float f32;
typedef double f64;
typedef unsigned char uchar;

typedef uintptr_t umm;

#define Kilobytes(value) (value * 1024)
#define Megabytes(value) (Kilobytes(value) * 1024)
#define Gigabytes(value) (Megabytes(value) * 1024)
#define ArrayCount(array) (sizeof(array)/sizeof(array[0]))

#define internal static
#define global static
#define local_persist static

#define Assert(stmt) if(!stmt) {*((int *) 0);}
struct Rectf
{
    f32 x;
    f32 y;
    f32 width;
    f32 height;
};

struct Rect
{
    i32 x;
    i32 y;
    i32 width;
    i32 height;
};

union v4
{
    struct
    {
        f32 r,g,b,a;
    };
    struct
    {
        f32 x,y,z,w;  
    };
};

struct v2
{
    f32 x,y;
};

v2 operator + (v2 left, v2 right)
{
    v2 result;
    result.x = left.x + right.x;
    result.y = left.y + right.y;
    return result;
}

v2 operator - (v2 left, v2 right)
{
    v2 result;
    result.x = left.x - right.x;
    result.y = left.y - right.y;
    return result;
}

v2 operator * (v2 left, f32 right)
{
    v2 result;
    result.x = left.x * right;
    result.y = left.y * right;
    return result;
}

v2 operator * (f32 left, v2 right)
{
    v2 result;
    result.x = left * right.x;
    result.y = left * right.y;
    return result;
}

v2 operator / (v2 left, f32 right)
{
    v2 result;
    result.x = left.x / right;
    result.y = left.y / right;
    return result;
}

v2 operator += (v2 &left, v2 right)
{
    left = left + right; 
    return left;
}

v2 operator -= (v2 &left, v2 right)
{
    left = left - right; 
    return left;
}

v2 operator - (v2 vector)
{
    v2 result;
    result = -1.0f * vector; 
    return result;
}

f32 Norm(v2 vector)
{
    f32 result = 0;
    
    result = sqrt(vector.x*vector.x + vector.y*vector.y);
    
    return result;
}

v2 Normalize(v2 vector)
{
    v2 result = {};
    
    f32 norm = Norm(vector);
    if(norm != 0)
    {
        result = vector * (1.0f/norm);
    }
    
    return result; 
}

//TODO: This is not production ready
i32 Round(f32 value)
{
    return (i32)(value + 0.5f);
}
