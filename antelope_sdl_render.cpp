void RenderListToOutput(SDL_Renderer *renderer, RenderList *rlist)
{
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    
    
    for(u32 i = 0; i < rlist->num_entries; i++)
    {
        RenderEntry *entry = &rlist->entries[i];
        switch(entry->type)
        {
        
            case Render_rectangle:
            {
                v4 color = entry->color;
                u32 r = Round(color.r * 255);
                u32 g = Round(color.b * 255);
                u32 b = Round(color.g * 255);
                u32 a = Round(color.a * 255);
            
                
                // TOOD: Consider using SDL version 2.0.10 so that we can have float-precision rectangle drawing?
                
                SDL_Rect sdl_rect = {};
                sdl_rect.x = Round(entry->rect.x);
                sdl_rect.y = Round(entry->rect.y);
                sdl_rect.w = Round(entry->rect.width);
                sdl_rect.h = Round(entry->rect.height);
                
                SDL_SetRenderDrawColor(renderer, r, g, b, a);
                SDL_RenderFillRect(renderer, &sdl_rect);
            } break; 
            
            case Render_texture:
            {
                RenderTexture *texture = &entry->texture;
                if(texture->gpu_handle == 0)
                {
                    u32 format = SDL_PIXELFORMAT_RGBA8888;
                    int access = SDL_TEXTUREACCESS_STREAMING;
                    u32 width = texture->width;
                    u32 height = texture->height;
                    texture->gpu_handle = SDL_CreateTexture(renderer, format, access, width, height);
                    
                    SDL_Rect update_rect = {};
                    // update_rect.width =
                    // SDL_UpdateTexture(entry->gpu_handle, )
                }
            } break;
            
            default:
            {
            } break;
            
        }
    }
    
    SDL_RenderPresent(renderer);        
}
